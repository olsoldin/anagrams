# README #

### What does it do? ###

It takes a word, and returns the anagrams for that word in the wordlist


### Running ###

[Download](https://bitbucket.org/olsoldin/anagrams/downloads) the latest version, unzip and run

`java -jar Anagrams.jar`

### Development Dependencies ###

This project requires [javax.json](https://repo1.maven.org/maven2/org/glassfish/javax.json/) for development