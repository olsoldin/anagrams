/* 
 * The MIT License
 *
 * Copyright 2016 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package anagrams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;

/**
 * Given a word, it will find all the anagrams of that word in the wordlist.
 * <p>
 * It works by taking a word list, for each word it sorts it alphabetically and
 * uses that as the key to store it in a dictionary.
 * <p>
 * An example of this would be:
 * <p>
 * "are" becomes "aer"
 * <p>
 * "aer": ["are"]
 * <p>
 * "ear" becomes "aer"
 * <p>
 * "aer": ["are","ear"]
 * <p>
 * "era" becomes "aer"
 * <p>
 * "aer": ["are","ear","era"]
 * <p>
 * Input word "ear"
 * <p>
 * output ["are","ear","era"]
 * <p>
 * @author Oliver
 * @version 1.0.0
 */
public class Dictionary {

	private static Dictionary instance = null;
	private HashMap<String, ArrayList<String>> dict = new HashMap<>();
	private boolean dictLoaded = false;

	private Dictionary() {
	}

	/**
	 * Only one instance of Dictionary
	 * <p>
	 * @return Dictionary object
	 */
	public static Dictionary getInstace() {
		if (instance == null) {
			instance = new Dictionary();
		}
		return instance;
	}

	/**
	 * Takes a wordlist, and for each word it orders the letters alphabetically
	 * and puts them in a dictionary
	 * <p>
	 * @param wordListPath   The path of the wordlist file
	 * @param dictionaryPath The path to save the dictionary to
	 * <p>
	 * @throws IOException If there is an error reading/writing to/from the
	 *                     files
	 */
	public void generateDictionaryFromWordlist(String wordListPath, String dictionaryPath) throws IOException {
		JsonArray jsonArray = readJsonFile(wordListPath);

		for (int i = 0; i < jsonArray.size(); i++) {
			// Get the word from the array
			String word = jsonArray.getString(i);
			
			// Sort the letters in the word alphabetically
			char[] wordChar = word.toCharArray();
			Arrays.sort(wordChar);
			String sortedWord = new String(wordChar);

			// Get the ArrayList of words associated with that sorted word
			ArrayList<String> anagrams = new ArrayList<>();
			if (dict.containsKey(sortedWord)) {
				anagrams = dict.get(sortedWord);
			}
			// Add the unsorted word to that ArrayList
			anagrams.add(word);
			// Update the disctionary with the new ArrayList
			dict.put(sortedWord, anagrams);
		}

		// Make sure we save the dictionart to the disk
		saveDictionary(dictionaryPath);
		// And let the program know we've got a dictionary loaded
		dictLoaded = true;
	}

	/**
	 * Returns the anagrams for a given word. The characters in the word are
	 * sorted alphabetically, then retrieved from a pre-generated dictionary of
	 * alphabetically sorted words.
	 * <p>
	 * @param word The word to find anagrams for
	 * <p>
	 * @return An Arraylist&lt;String&gt; of anagrams for the word. Empty if no
	 *         anagrams
	 * <p>
	 * @throws java.lang.Exception The dictionary isn't loaded.
	 */
	public ArrayList<String> getAnagrams(String word) throws Exception {
		if(!dictLoaded){
			throw new Exception("Dictionary not loaded.\nTry calling generateDictionaryFromWordlist(..) or loadDictionary(..) first.");
		}
		// Sort the characters in the word alphabetically
		char[] sortedWord = word.toCharArray();
		Arrays.sort(sortedWord);
		// Use that sorted word as a key to get the anagrams ArrayList
		String key = new String(sortedWord);
		if (dict.containsKey(key)) {
			return dict.get(key);
		}
		// no anagrams in the dictionary
		return new ArrayList<>();
	}

	/**
	 * Get the dictionary as a hashmap
	 * <p>
	 * @return the dictionary as a HashMap&lt;Sring, ArrayList&lt;String&gt;&gt;
	 */
	public HashMap<String, ArrayList<String>> getDictionary() {
		return dict;
	}

	/**
	 * Returns true if the dictionary has been loaded from memory if false,
	 * loadDictionary() should be called
	 * <p>
	 * @return True if the dictionary has been loaded into the program
	 */
	public boolean isDictionaryLoaded() {
		return dictLoaded;
	}

	/**
	 * Loads the dictionary HashMap from the file given
	 * <p>
	 * @param path The location of the saved dictionary
	 * <p>
	 * @throws IOException            If the file cannot be found/read from
	 * @throws ClassNotFoundException If the file doesn't contain a
	 *                                HashMap&lt;String,
	 *                                ArrayList&lt;String&gt;&gt;
	 */
	public void loadDictionary(String path) throws IOException, ClassNotFoundException {
		// Start a fresh dictionary
		dict = new HashMap<>();

		File file = new File(path);
		try (InputStream in = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(in)) {
			dict = (HashMap<String, ArrayList<String>>) ois.readObject();
		}
		dictLoaded = true;
	}

	/**
	 * Reads the json file and returns it as a JsonArray The file has to be a
	 * json array only eg.
	 * <p>
	 * ["item1","item2","item3"]
	 * <p>
	 * @param path The path of the file to read
	 * <p>
	 * @return The file as a JsonArray object
	 * <p>
	 * @throws IOException if the file it not found, or there is an issue
	 *                     reading it
	 */
	private JsonArray readJsonFile(String path) throws IOException {
		File file = new File(path);
		JsonArray jsonArray;
		try (InputStream fis = new FileInputStream(file);
				JsonReader jsonReader = Json.createReader(fis)) {
			jsonArray = jsonReader.readArray();
		}

		return jsonArray;
	}

	/**
	 * Saves the dictionary HashMap to a file
	 * <p>
	 * @param path The path of the file to save the HashMap dictionary to
	 * <p>
	 * @throws IOException If there is a problem reading/writing to/from the
	 *                     file
	 */
	private void saveDictionary(String path) throws IOException {
		File file = new File(path);
		try (OutputStream out = new FileOutputStream(file);
				ObjectOutputStream oos = new ObjectOutputStream(out)) {
			oos.writeObject(dict);
		}
	}
}
