/* 
 * The MIT License
 *
 * Copyright 2016 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package anagrams;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Entry and IO class
 * <p>
 * @author Oliver
 * @version 1.0.1
 */
public class Anagrams {

	// Default values (in same directory)
	private static String wordListPath = "./words.json";
	private static String dictPath = "./dict.obj";

	/**
	 * @param args the command line arguments (unused)
	 */
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(Dictionary.class.getName());

		Dictionary dict = Dictionary.getInstace();

		Scanner scanner = new Scanner(System.in);
		// Make sure the scanner is properly closed when we end the program
		Runtime.getRuntime().addShutdownHook(new Thread(scanner::close));

		boolean generate = getInput("Generate dictionary from wordlist?", false, scanner);
		if (generate) {
			wordListPath = getReadPath("Enter the wordlist path:", wordListPath, scanner);

			dictPath = getInput("Enter the file to save the dictionary to:", dictPath, scanner);

			try {
				dict.generateDictionaryFromWordlist(wordListPath, dictPath);
			} catch (IOException ex) {
				logger.log(Level.SEVERE, "Error generating the dictionary", ex);
				// Don't continue if there was an error
				return;
			}
		}

		// Generating the dictionary also loads it at the same time
		// So this is in case we didn't generate it.
		if (!dict.isDictionaryLoaded()) {
			dictPath = getReadPath("Enter the dictionary file:", dictPath, scanner);
			try {
				System.out.println("Loading dictionary...");
				dict.loadDictionary(dictPath);
				System.out.println("Dictionary loaded.");
			} catch (IOException | ClassNotFoundException ex) {
				logger.log(Level.SEVERE, "Error loading the dictionary", ex);
				// Don't continue if there was an error
				return;
			}
		}

		boolean restart = true;
		while (restart) {
			String word = getInput("Enter the word to find anagrams for:", "", scanner);

			try {
				System.out.println(dict.getAnagrams(word));
			} catch (Exception ex) {
				Logger.getLogger(Anagrams.class.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("");

			restart = getInput("Another word?", true, scanner);
		}
	}

	/**
	 * Only returns when the user's input leads to a valid file
	 * <p>
	 * @param message The message to show the user (The default input is
	 *                appended to this)
	 * @param def     The default input if no input is given
	 * @param scanner The scanner to use
	 * <p>
	 * @return A valid path from the user, or the default if no input is given
	 */
	private static String getReadPath(String message, String def, Scanner scanner) {
		while (true) {
			String input = getInput(message, def, scanner);
			File tmp = new File(input);
			if (tmp.exists()) {
				return input;
			}
			StringBuilder sb = new StringBuilder();

			sb.append(Colour.BG_RED.c("Error: File "));
			sb.append(Colour.FG_CYAN.c(input));
			sb.append(Colour.BG_RED.c(" Doesn't exist. Try again."));

			System.out.println(sb);
		}
	}

	/**
	 * Gets a string input from the user.
	 * <p>
	 * @param message The message to show the user (The default input is
	 *                appended to this)
	 * @param def     The default input if no input is given
	 * @param scanner The scanner to use
	 * <p>
	 * @return The user input given, or the default if none is given
	 */
	private static String getInput(String message, String def, Scanner scanner) {
		StringBuilder sb = new StringBuilder(message);
		if (!"".equals(def)) {
			sb.append(" (default: \"");
			sb.append(def);
			sb.append("\")");
		}
		sb.append("\n>");
		System.out.print(sb);

		String tmp = nextLineFrom(scanner);

		if ("".equals(tmp)) {
			return def;
		}
		return tmp;
	}

	/**
	 * Gets a boolean input from the user. valid: y yes n no
	 * <p>
	 * @param message The message to show the user (the default input is
	 *                appended to this)
	 * @param def     The default input if no/an invalid input is given
	 * @param scanner The scanner to use
	 * <p>
	 * @return true of false, depending on the user input
	 */
	private static boolean getInput(String message, boolean def, Scanner scanner) {
		StringBuilder sb = new StringBuilder(message);
		if (def) {
			sb.append(" (Y/n)");
		} else {
			sb.append(" (y/N)");
		}
		sb.append("\n>");
		System.out.print(sb);

		String tmp = nextLineFrom(scanner).toLowerCase();

		if (null != tmp) {
			switch (tmp) {
				case "n":
				case "no":
					return false;
				case "y":
				case "yes":
					return true;
			}
		}

		return def;
	}

	/**
	 * return scanner.nextLne(), except catching the NoSuchElementExcepton that
	 * gets thrown when ctrl-c is pressed
	 * <p>
	 * @param scanner the scanner to use
	 * <p>
	 * @return the next user input
	 */
	private static String nextLineFrom(Scanner scanner) {
		try {
			return scanner.nextLine().trim();
		} catch (NoSuchElementException ex) {
			Logger.getLogger(Anagrams.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "";
	}

	/**
	 * Private constructor because this class is only an entry point
	 */
	private Anagrams() {
	}

	/**
	 * Something simple to colourise the output
	 */
	enum Colour {

		RESET("\033[0m"), HICOLOR("\033[1m"), UNDERLINE("\033[4m"), INVERSE("\033[7m"),
		FG_BLACK("\033[30m"), FG_RED("\033[31m"), FG_GREEN("\033[32m"), FG_YELLOW("\033[33m"), FG_BLUE("\033[34m"), FG_MAGENTA("\033[35m"), FG_CYAN("\033[36m"), FG_WHITE("\033[37m"),
		BG_BLACK("\033[40m"), BG_RED("\033[41m"), BG_GREEN("\033[42m"), BG_YELLOW("\033[43m"), BG_BLUE("\033[44m"), BG_MAGENTA("\033[45m"), BG_CYAN("\033[46m"), BG_WHITE("\033[47m");

		private final String code;

		Colour(String code) {
			this.code = code;
		}

		/**
		 * Returns the given string in the current style eg.
		 * Colour.FG_BLUE.c("Hello"); returns the string "Hello" with the fg
		 * colour as blue
		 * <p>
		 * @param string The string to stylise
		 * <p>
		 * @return The param string sylised as in the current style
		 */
		String c(String string) {
			return string;
			//return this + string + Colour.RESET;
		}

		@Override
		public String toString() {
			return this.code;
		}
	}
}
